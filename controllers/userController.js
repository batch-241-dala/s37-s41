const User = require("../model/Users");
const Course = require("../model/Course")
const bcryptjs = require('bcryptjs');
const auth = require('../auth');


module.exports.checkEmailExists = (reqBody) => {

	return User.find({email:reqBody.email}).then( result => {

		if (result.length > 0) {
			return true
		// no duplicate email found
		} else {
			return false
		}

	});

};


module.exports.registerUser = (reqBody) => {

	let newUser = new User ({

		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcryptjs.hashSync(reqBody.password, 10), // bcrypt.hashSync(<dataToBeHash>,<saltRound>)
		mobileNo: reqBody.mobileNo

	})

	return newUser.save().then((user, error) => {

		if (error) {
			return false
		} else {
			return user
		}

	})

};

module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		
		// console.log(result)
		if(result == null) {
			
			return false
		
		} else {

			// compareSync(dataFromRequestBody, encryptedDataFromDatabase)
			const isPasswordCorrect = bcryptjs.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				
				return { access: auth.createAccessToken(result) }
			
			} else {
				
				return false
			
			};
		};
	});

};

module.exports.getProfile = (reqBody) => {

	return User.findById(reqBody.id).then(result => {

		if(result == null) {
			
			return false
		
		} else {

			result.password = "";
			return result;

		}

	})

}

module.exports.enroll = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId: data.courseId});
		return user.save().then((user, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
			course.enrollees.push({userId: data.userId});
			return course.save().then((course, error) => {
				if(error){
					return false
				} else {
					return true
				}
			})
	})

	if(isUserUpdated && isCourseUpdated){
		return true
	} else {
		return false
	}

}