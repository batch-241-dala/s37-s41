const Course = require('../model/Course');
const User = require('../model/Users');
const auth = require('../auth');

module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price

	})

	return newCourse.save().then((course, error) => {

		if(error) {

			return false

		} else {

			return true
			

		}

	})

}

// Retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	});
};


// Retrieve ACTIVE COURSES
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};


// Retrieve specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
};


module.exports.updateCourse = (reqParams, reqBody) => {
	
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	// Syntax: findByIdAndUpdate(document ID, udpatesToBeApplied)

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
			if(error){
				return false;
			} else {
				return true;
			};

	});

};


module.exports.archiveCourse = (reqParams, reqBody) => {

	let archivedCourse = {
		isActive: reqBody.isActive
	};

	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {

		if(error){
			return false;
		} else {
			return "Archived success"
		}

	})

}