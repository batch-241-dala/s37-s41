const jwt = require('jsonwebtoken');

const secret = 'CourseBookingAPI';

module.exports.createAccessToken = (user) => {

	// object that was passed on our controller, "result"
	/*{
	  _id: new ObjectId("63dc65c173a78e807ac9dc42"),
	  firstName: 'john',
	  lastName: 'doe',
	  email: 'jdoe@mail.com',
	  password: '$2a$10$/zPDXU/0GqSElMkq7TtXlOpcf5NFwKOr6Z2E6JjrN9r5S5ppXV7c.',
	  isAdmin: false,
	  mobileNo: '5529321',
	  enrollments: [],
	  __v: 0
	}*/

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};


	// jwt.sign(data/payload, secretkey, options);
	return jwt.sign(data, secret, {});

}


// Token Verification

module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;

	if(typeof token !== "undefined") {
		console.log(token);

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {

			if(error) {
				return res.send({auth: "failed"});
			} else {

				next();
			};

		});

	} else {

		return res.send({auth: "failed"});

	};

};


// Token Decryption
module.exports.decode = (token) => {

	if(typeof token !== "undefined") {
		token = token.slice(7, token.length);
		
		return jwt.verify(token, secret, (error, data) => {

			if(error){
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload;
			}

		});
	} else {
		return null
	}

};